package com.example.fooddataviewer

import com.example.fooddataviewer.di.DaggerTestComponent


class AndroidTestApplication : Application() {

    override val component by lazy {
        DaggerTestComponent.builder()
            .context(this)
            .build()
    }
}
