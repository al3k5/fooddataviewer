package com.example.fooddataviewer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.fooddataviewer.utils.ActivityService
import com.google.firebase.FirebaseApp

class Activity : AppCompatActivity() {

    private lateinit var activityService: ActivityService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FirebaseApp.initializeApp(applicationContext)
        activityService = applicationComponent.activityService()
        activityService.onCreate(this)
        setContentView(R.layout.activity)
    }

    override fun onDestroy() {
        activityService.onDestroy(this)
        super.onDestroy()
    }
}
