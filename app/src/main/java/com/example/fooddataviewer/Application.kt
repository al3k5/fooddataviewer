package com.example.fooddataviewer

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.example.fooddataviewer.di.ApplicationComponent
import com.example.fooddataviewer.di.DaggerApplicationComponent
import kotlin.reflect.KClass
import android.app.Application as AndroidApplication

open class Application : AndroidApplication() {

    open val component by lazy {
        DaggerApplicationComponent.builder()
            .context(this)
            .build()
    }
}

val Context.applicationComponent: ApplicationComponent
    get() = (this.applicationContext as Application).component

fun <T, M, E> Fragment.getViewModel(type: KClass<T>): ViewModelInt<M, E>
        where T : ViewModel,
              T : ViewModelInt<M, E> {
    val factory = (this.context!!.applicationContext as Application).component.viewModelFactory()
    return ViewModelProviders.of(this, factory)[type.java]
}

fun <T, M, E> FragmentActivity.getViewModel(type: KClass<T>): ViewModelInt<M, E>
        where T : ViewModel,
              T : ViewModelInt<M, E> {
    val factory = (this.applicationContext as Application).component.viewModelFactory()
    return ViewModelProviders.of(this, factory)[type.java]
}
