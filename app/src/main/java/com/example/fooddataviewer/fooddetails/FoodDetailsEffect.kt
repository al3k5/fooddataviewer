package com.example.fooddataviewer.fooddetails

import com.example.fooddataviewer.model.Product

sealed class FoodDetailsEffect

data class LoadProduct(val code: String) : FoodDetailsEffect()

data class SaveProductEffect(val product: Product) : FoodDetailsEffect()

data class DeleteProductEffect(val code: String) : FoodDetailsEffect()
