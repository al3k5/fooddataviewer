package com.example.fooddataviewer.fooddetails

import com.example.fooddataviewer.model.Product

sealed class FoodDetailsEvent

data class Initial(val code: String) : FoodDetailsEvent()

data class ProductLoaded(val product: Product) : FoodDetailsEvent()

object ActionButtonClicked : FoodDetailsEvent()
