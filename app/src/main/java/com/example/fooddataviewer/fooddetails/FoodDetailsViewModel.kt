package com.example.fooddataviewer.fooddetails

import com.example.fooddataviewer.BaseViewModel
import com.example.fooddataviewer.model.ProductRepository
import com.spotify.mobius.Next
import com.spotify.mobius.Next.dispatch
import com.spotify.mobius.Next.next
import com.spotify.mobius.Next.noChange
import com.spotify.mobius.Update
import com.spotify.mobius.rx2.RxMobius
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

fun foodDetailsUpdate(
    model: FoodDetailsModel,
    event: FoodDetailsEvent
): Next<FoodDetailsModel, FoodDetailsEffect> {
    return when (event) {
        is Initial -> next(
            model.copy(activity = true),
            setOf(LoadProduct(event.code))
        )
        is ProductLoaded -> next(model.copy(activity = false, product = event.product))
        is ActionButtonClicked -> if (model.product != null) {
            if (model.product.saved) {
                dispatch<FoodDetailsModel, FoodDetailsEffect>(
                    setOf(DeleteProductEffect(model.product.id))
                )
            } else {
                dispatch<FoodDetailsModel, FoodDetailsEffect>(
                    setOf(SaveProductEffect(model.product))
                )
            }
        } else {
            noChange()
        }
    }
}

class FoodDetailsViewModel @Inject constructor(
    productRepository: ProductRepository
) : BaseViewModel<FoodDetailsModel, FoodDetailsEvent, FoodDetailsEffect>(
    "FoodDetailsViewModel",
    Update(::foodDetailsUpdate),
    FoodDetailsModel(),
    RxMobius.subtypeEffectHandler<FoodDetailsEffect, FoodDetailsEvent>()
        .addTransformer(LoadProduct::class.java) { upstream ->
            upstream.switchMap { effect ->
                productRepository.loadProduct(effect.code)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .toObservable()
                    .map { product -> ProductLoaded(product) as FoodDetailsEvent }
            }
        }
        .addTransformer(SaveProductEffect::class.java) { upstream ->
            upstream.switchMap { effect ->
                productRepository.saveProduct(effect.product)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .toObservable<FoodDetailsEvent>()
            }
        }
        .addTransformer(DeleteProductEffect::class.java) { upstream ->
            upstream.switchMap { effect ->
                productRepository.deleteProduct(effect.code)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .toObservable<FoodDetailsEvent>()
            }
        }
        .build()
)
