package com.example.fooddataviewer.foodlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.fooddataviewer.R
import com.example.fooddataviewer.foodlist.widget.FoodListAdapter
import com.example.fooddataviewer.getViewModel
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.food_list_fragment.*

class FoodListFragment : Fragment() {

    private lateinit var disposable: Disposable

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.food_list_fragment, container, false)
    }

    override fun onStart() {
        super.onStart()

        recyclerView.layoutManager = LinearLayoutManager(context)
        val adapter = FoodListAdapter()
        recyclerView.adapter = adapter

        disposable = Observable.mergeArray(addButton.clicks().map { AddButtonClicked },
            adapter.productClicks.map { ProductClicked(it) })
            .compose(getViewModel(FoodListViewModel::class).init(Initial))
            .subscribe { model ->
                adapter.submitProductList(model.products)
            }
    }

    override fun onDestroyView() {
        disposable.dispose()
        super.onDestroyView()
    }
}
