package com.example.fooddataviewer.model

import com.example.fooddataviewer.model.api.ProductService
import com.example.fooddataviewer.model.database.ProductDao
import com.example.fooddataviewer.model.dto.NutrimentsDto
import com.example.fooddataviewer.model.dto.ProductDto
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class ProductRepository @Inject constructor(
    private val productService: ProductService,
    private val productDao: ProductDao
) {

    fun get(): Observable<List<Product>> {
        return productDao.get()
            .map { products -> mapProducts(products) }
    }

    fun loadProduct(barcode: String): Single<Product> {
        return getProductFromDatabase(barcode)
            .onErrorResumeNext(getProductFromApi(barcode))
    }

    fun getProductFromDatabase(barcode: String): Single<Product> {
        return productDao.getProduct(barcode)
            .map { product ->
                mapProduct(product, true)
            }
    }

    fun getProductFromApi(barcode: String): Single<Product> {
        return productService.getProduct(barcode)
            .map { response -> mapProduct(response.product, false) }
    }

    fun saveProduct(product: Product): Completable {
        return Single.fromCallable { mapProductDto(product) }
            .flatMapCompletable { productDao.insert(it) }
    }

    fun deleteProduct(barcode: String): Completable {
        return productDao.delete(barcode)
    }
}

private fun mapProductDto(product: Product): ProductDto {
    return ProductDto(
        id = product.id,
        product_name = product.name,
        brands = product.brands,
        image_url = product.imageUrl,
        ingredients_text_debug = product.ingridients,
        nutriments = mapNutrimentsDto(product.nutriments)
    )
}

private fun mapNutrimentsDto(nutriments: Nutriments?): NutrimentsDto? {
    if (nutriments == null) return null
    return NutrimentsDto(
        energy_100g = nutriments.energy,
        salt_100g = nutriments.salt,
        carbohydrates_100g = nutriments.carbohydrates,
        fiber_100g = nutriments.fiber,
        sugars_100g = nutriments.sugars,
        proteins_100g = nutriments.proteins,
        fat_100g = nutriments.fat
    )
}

private fun mapProducts(products: List<ProductDto>): List<Product> {
    val list = mutableListOf<Product>()
    for (dto in products) {
        list.add(mapProduct(dto, true))
    }
    return list
}

private fun mapProduct(dto: ProductDto, saved: Boolean): Product {
    return Product(
        id = dto.id,
        saved = saved,
        name = dto.product_name,
        brands = dto.brands,
        imageUrl = dto.image_url,
        ingridients = dto.ingredients_text_debug,
        nutriments = mapNutrients(dto.nutriments)
    )
}

private fun mapNutrients(dto: NutrimentsDto?): Nutriments {
    if (dto == null) {
        return Nutriments(
            energy = 0,
            salt = 0.0,
            carbohydrates = 0.0,
            fiber = 0.0,
            sugars = 0.0,
            proteins = 0.0,
            fat = 0.0
        )
    }
    return Nutriments(
        energy = dto.energy_100g,
        salt = dto.salt_100g ?: 0.0,
        carbohydrates = dto.carbohydrates_100g,
        fiber = dto.fiber_100g ?: 0.0,
        sugars = dto.sugars_100g ?: 0.0,
        proteins = dto.sugars_100g ?: 0.0,
        fat = dto.fat_100g
    )
}
