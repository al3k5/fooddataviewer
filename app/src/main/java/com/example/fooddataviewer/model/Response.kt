package com.example.fooddataviewer.model

import com.example.fooddataviewer.model.dto.ProductDto
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Response(
    val code: String,
    val product: ProductDto
)
