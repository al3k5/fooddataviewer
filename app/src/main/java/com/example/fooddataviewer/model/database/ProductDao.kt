package com.example.fooddataviewer.model.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.fooddataviewer.model.dto.ProductDto
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

@Dao
abstract class ProductDao {

    @Query("SELECT * FROM ProductDto")
    abstract fun get(): Observable<List<ProductDto>>

    @Query("SELECT * FROM ProductDto WHERE id=:code")
    abstract fun getProduct(code: String): Single<ProductDto>

    @Insert
    abstract fun insert(productDto: ProductDto): Completable

    @Query("DELETE FROM productdto WHERE id= :code")
    abstract fun delete(code: String): Completable
}
