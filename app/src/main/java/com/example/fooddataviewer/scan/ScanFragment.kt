package com.example.fooddataviewer.scan

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import android.os.Bundle
import android.util.Log
import android.util.SparseIntArray
import android.view.Surface
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.fooddataviewer.R
import com.example.fooddataviewer.applicationComponent
import com.example.fooddataviewer.getViewModel
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import com.jakewharton.rxbinding3.view.clicks
import io.fotoapparat.Fotoapparat
import io.fotoapparat.configuration.CameraConfiguration
import io.fotoapparat.selector.continuousFocusPicture
import io.fotoapparat.selector.manualExposure
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.product_layout_small.*
import kotlinx.android.synthetic.main.scan_fragment.*

class ScanFragment : Fragment(R.layout.scan_fragment) {

    private val orientations = SparseIntArray()

    init {
        orientations.append(Surface.ROTATION_0, 90)
        orientations.append(Surface.ROTATION_90, 0)
        orientations.append(Surface.ROTATION_180, 270)
        orientations.append(Surface.ROTATION_270, 180)
    }

    private lateinit var fotoapparat: Fotoapparat

    private lateinit var disposable: Disposable

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val frameProcessor = context!!.applicationComponent.frameProcessorOnSubscribe()
        fotoapparat = Fotoapparat(
            context = requireContext(),
            view = cameraView,
            cameraConfiguration = CameraConfiguration(
                exposureCompensation = manualExposure(4),
                focusMode = continuousFocusPicture(),
                frameProcessor = frameProcessor
            )
        )

        val cameraId = findRearFacingCameraId()

        disposable =
            Observable.mergeArray(
                Observable.create(frameProcessor)
                    .map { frame ->
                        Captured(
                            frame.copy(
                                rotation =
                                getRotationCompensation(
                                    cameraId,
                                    this.activity as Activity,
                                    this.context!!
                                )
                            )
                        )
                    },
                productView.clicks().map { ProductInfoClicked }
            )
                .compose(getViewModel(ScanViewModel::class))
                .subscribe { model ->
                    loadingIndicator.isVisible = model.activity
                    productView.isVisible =
                        model.processBarcodeResult is ProcessBarcodeResult.BarcodeLoaded
                    errorView.isVisible =
                        model.processBarcodeResult is ProcessBarcodeResult.Error

                    if (model.processBarcodeResult is ProcessBarcodeResult.BarcodeLoaded) {
                        productNameView.text = model.processBarcodeResult.product.name
                        brandNameView.text = model.processBarcodeResult.product.brands
                        energyValue.text = getString(
                            R.string.scan_energy_value,
                            model.processBarcodeResult.product.nutriments?.energy
                        )
                        carbsValueView.text = getString(
                            R.string.scan_macro_value,
                            model.processBarcodeResult.product.nutriments?.carbohydrates
                        )
                        fatValueView.text = getString(
                            R.string.scan_macro_value,
                            model.processBarcodeResult.product.nutriments?.fat
                        )
                        proteinValue.text = getString(
                            R.string.scan_macro_value,
                            model.processBarcodeResult.product.nutriments?.proteins
                        )

                        Glide.with(requireContext())
                            .load(model.processBarcodeResult.product.imageUrl)
                            .fitCenter()
                            .into(productImageView)
                    }
                }
    }

    override fun onDestroyView() {
        disposable.dispose()
        super.onDestroyView()
    }

    override fun onStart() {
        super.onStart()
        handleCameraPermission(false)
    }

    override fun onStop() {
        fotoapparat.stop()
        super.onStop()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        handleCameraPermission(true)
    }

    private fun hasCameraPermission() =
        ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED

    private fun handleCameraPermission(permissionsResult: Boolean) {
        if (hasCameraPermission()) {
            fotoapparat.start()
        } else if (!permissionsResult ||
            shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)
        ) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), 0)
        }
    }

    private fun findRearFacingCameraId(): String {
        val cameraManager = activity?.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        val cameraIds = cameraManager.cameraIdList
        for (id: String in cameraIds) {
            val characteristics = cameraManager.getCameraCharacteristics(id)
            val orientation = characteristics.get(CameraCharacteristics.LENS_FACING)
            if (orientation == CameraCharacteristics.LENS_FACING_BACK) return id
        }
        throw IllegalStateException("Unable to find camera id")
    }

    private fun getRotationCompensation(
        cameraId: String,
        activity: Activity,
        context: Context
    ): Int {
        val deviceRotation = activity.windowManager.defaultDisplay.rotation
        var rotationCompensation = orientations.get(deviceRotation)

        val cameraManager = context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        val sensorOrientation = cameraManager
            .getCameraCharacteristics(cameraId)
            .get(CameraCharacteristics.SENSOR_ORIENTATION)!!
        rotationCompensation = (rotationCompensation + sensorOrientation + 270) % 360

        val result: Int
        when (rotationCompensation) {
            0 -> result = FirebaseVisionImageMetadata.ROTATION_0
            90 -> result = FirebaseVisionImageMetadata.ROTATION_90
            180 -> result = FirebaseVisionImageMetadata.ROTATION_180
            270 -> result = FirebaseVisionImageMetadata.ROTATION_270
            else -> {
                result = FirebaseVisionImageMetadata.ROTATION_0
                Log.e("OrderCheckInFragment", "Bad rotation value: $rotationCompensation")
            }
        }
        return result
    }
}
