package com.example.fooddataviewer.scan

import com.example.fooddataviewer.model.Product

data class ScanModel(
    val activity: Boolean = false,
    val processBarcodeResult: ProcessBarcodeResult = ProcessBarcodeResult.Empty
)

sealed class ProcessBarcodeResult {
    object Empty : ProcessBarcodeResult()
    object Error : ProcessBarcodeResult()
    data class BarcodeLoaded(val product: Product) : ProcessBarcodeResult()
}
