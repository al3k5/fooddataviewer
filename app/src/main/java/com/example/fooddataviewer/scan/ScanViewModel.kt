package com.example.fooddataviewer.scan

import com.example.fooddataviewer.BaseViewModel
import com.example.fooddataviewer.scan.handler.ProcessBarcodeHandler
import com.example.fooddataviewer.scan.handler.ProcessFrameHandler
import com.example.fooddataviewer.utils.Navigator
import com.spotify.mobius.Next
import com.spotify.mobius.Next.dispatch
import com.spotify.mobius.Next.next
import com.spotify.mobius.Next.noChange
import com.spotify.mobius.Update
import com.spotify.mobius.rx2.RxMobius
import javax.inject.Inject

fun scanUpdate(
    model: ScanModel,
    event: ScanEvent
): Next<ScanModel, ScanEffect> {
    return when (event) {
        is Captured -> dispatch(setOf(ProcessCameraFrame(event.frame)))
        is Detected -> if (!model.activity) {
            next<ScanModel, ScanEffect>(
                model.copy(activity = true),
                setOf(ProcessBarcode(event.barcode))
            )
        } else {
            noChange<ScanModel, ScanEffect>()
        }
        is BarcodeError -> next(
            model.copy(
                activity = false,
                processBarcodeResult = ProcessBarcodeResult.Error
            )
        )
        is ProductLoaded -> next(
            model.copy(
                activity = false,
                processBarcodeResult = ProcessBarcodeResult.BarcodeLoaded(event.product)
            )
        )
        is ProductInfoClicked ->
            if (model.processBarcodeResult is ProcessBarcodeResult.BarcodeLoaded) {
                dispatch<ScanModel, ScanEffect>(
                    setOf(NavigateToFoodDetails(model.processBarcodeResult.product.id))
                )
            } else {
                noChange<ScanModel, ScanEffect>()
            }
    }
}

class ScanViewModel @Inject constructor(
    processFrameHandler: ProcessFrameHandler,
    processBarcodeHandler: ProcessBarcodeHandler,
    navigator: Navigator
) : BaseViewModel<ScanModel, ScanEvent, ScanEffect>(
    "ScanViewModel",
    Update(::scanUpdate),
    ScanModel(),
    RxMobius.subtypeEffectHandler<ScanEffect, ScanEvent>()
        .addTransformer(ProcessCameraFrame::class.java, processFrameHandler)
        .addTransformer(ProcessBarcode::class.java, processBarcodeHandler)
        .addConsumer(NavigateToFoodDetails::class.java) { effect ->
            navigator.to(ScanFragmentDirections.foodDetails(effect.barcode))
        }
        .build()
)
