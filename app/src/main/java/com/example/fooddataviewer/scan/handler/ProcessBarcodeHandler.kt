package com.example.fooddataviewer.scan.handler

import android.util.Log
import com.example.fooddataviewer.model.ProductRepository
import com.example.fooddataviewer.scan.BarcodeError
import com.example.fooddataviewer.scan.ProcessBarcode
import com.example.fooddataviewer.scan.ProductLoaded
import com.example.fooddataviewer.scan.ScanEvent
import com.example.fooddataviewer.utils.IdlingResource
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProcessBarcodeHandler @Inject constructor(
    private val productRepository: ProductRepository,
    private val idlingResource: IdlingResource
) :
    ObservableTransformer<ProcessBarcode, ScanEvent> {

    override fun apply(upstream: Observable<ProcessBarcode>): ObservableSource<ScanEvent> {
        return upstream.switchMap { effect ->
            productRepository.getProductFromApi(effect.barcode)
                .map { product ->
                    idlingResource.decrement()
                    ProductLoaded(product) as ScanEvent
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError { error -> Log.d("Error", error.message, error) }
                .onErrorReturnItem(BarcodeError)
                .toObservable()
        }
    }
}
