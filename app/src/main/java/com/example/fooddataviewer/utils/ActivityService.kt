package com.example.fooddataviewer.utils

import android.app.Activity

class ActivityService {

    private var _activity: Activity? = null
    val activity: Activity
        get() = _activity!!

    fun back() {
        activity.onBackPressed()
    }

    fun onCreate(activity: Activity) {
        this._activity = activity
    }

    fun onDestroy(activity: Activity) {
        if (this._activity == activity) {
            this._activity = null
        }
    }
}
