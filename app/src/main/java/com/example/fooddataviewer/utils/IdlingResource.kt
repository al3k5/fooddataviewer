package com.example.fooddataviewer.utils

import androidx.test.espresso.idling.CountingIdlingResource
import com.example.fooddataviewer.BuildConfig

class IdlingResource {
    val testCountingIdlingResource = CountingIdlingResource("Effect")
    private var incremented = false

    fun decrement() {
        if (BuildConfig.BUILD_TYPE == "instrumented") {
            testCountingIdlingResource.decrement()
            incremented = false
        }
    }
}
