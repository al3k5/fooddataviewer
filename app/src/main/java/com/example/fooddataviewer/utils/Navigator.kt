package com.example.fooddataviewer.utils

import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.findNavController

class Navigator(private val viewId: Int, private val activityService: ActivityService) {

    private val navController: NavController
        get() = activityService.activity.findNavController(viewId)

    fun back(id: Int? = null, inclusive: Boolean = false) {
        if (id == null) {
            activityService.back()
        } else {
            navController.popBackStack(id, inclusive)
        }
    }

    fun to(directions: NavDirections) {
        navController.navigate(directions)
    }
}

